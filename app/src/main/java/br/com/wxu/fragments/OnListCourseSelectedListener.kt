package br.com.wxu.fragments

interface OnListCourseSelectedListener {
    fun onCourseSelected(id: Long)
}