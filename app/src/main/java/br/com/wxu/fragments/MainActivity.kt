package br.com.wxu.fragments

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class MainActivity : AppCompatActivity(), OnListCourseSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onCourseSelected(id: Long) {
        val courseDetailFragment = fragmentManager.findFragmentById(R.id.fragment_course_detail)

        if(courseDetailFragment !== null) {
            (courseDetailFragment as CourseDetailFragment).load(id.toInt())
        } else {
            // implement activity with fragment detail
        }
    }
}
