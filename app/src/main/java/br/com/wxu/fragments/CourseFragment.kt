package br.com.wxu.fragments

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView

class CourseFragment : Fragment(), AdapterView.OnItemClickListener {

    private val list by lazy { view.findViewById<ListView>(R.id.list_courses) }
    private var callback : OnListCourseSelectedListener? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_courses, container, false)
    }

    override fun onStart() {
        super.onStart()

        val courses = listOf("Android Basic", "Android Middle", "Android Advanced")
        val adapter =  ArrayAdapter<String>(activity, android.R.layout.simple_gallery_item, courses)

        list.adapter = adapter
        list.onItemClickListener = this
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callback = context as OnListCourseSelectedListener
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        callback?.onCourseSelected(id)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.fragment_courses_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }
}