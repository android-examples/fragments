package br.com.wxu.fragments

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_course_detail.*

class CourseDetailFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_course_detail, container, false)
    }

    fun load(id : Int) {
        val courses = listOf("Android Basic", "Android Middle", "Android Advanced")
        val selected = courses[id]

        text_course.text = selected
    }
}